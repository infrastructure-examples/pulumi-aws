"use strict";

let pulumi = require("@pulumi/pulumi");
let eks = require("@pulumi/eks");
let k8s = require("@pulumi/kubernetes");

let env = pulumi.getStack();

let gitlabStack = new pulumi.StackReference(`mkasa/gitlab/${env}`);

let cluster = new eks.Cluster("gitlab-aws-example-1");

let clusterNamespace = new k8s.core.v1.Namespace("gitlab-aws-example-staging", {
  metadata: {
    name: pulumi.interpolate `gitlab-aws-example-${gitlabStack.getOutput("projectId")}-${env}`,
  },
}, { provider: cluster.provider });

let serviceAccount = new k8s.core.v1.ServiceAccount("gitlab", {
  metadata: {
    name: "gitlab",
    namespace: "kube-system",
  },
}, { provider: cluster.provider });

let clusterRoleBinding = new k8s.rbac.v1.ClusterRoleBinding("gitlab-admin", {
  metadata: {
    name: "gitlab-admin",
  },
  subjects: [{
    kind: "ServiceAccount",
    name: serviceAccount.metadata.name,
    namespace: serviceAccount.metadata.namespace,
  }],
  roleRef: {
    kind: "ClusterRole",
    name: "cluster-admin",
    apiGroup: "rbac.authorization.k8s.io",
  },
}, { provider: cluster.provider });

let secret = new k8s.core.v1.Secret("gitlab-admin", {
  type: "kubernetes.io/service-account-token",
  metadata: {
    name: "gitlab-admin",
    annotations: {
      "kubernetes.io/service-account.name": serviceAccount.metadata.name,
    },
  },
}, { provider: cluster.provider });

exports.token = secret.stringData;
